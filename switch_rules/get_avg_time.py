import pandas as pd 

# get the average message send times for all trials
for t in range(9):
	try: 
		df = pd.read_csv(f"trial-{t}/measure.log", sep=';', names=['message_no', 'send_time','receive_time','channel'])
		df['send_duration'] = (df['receive_time']-df['send_time'])
		print(f"trial-{t}", df['send_duration'].mean())
	except: 
		pass 
