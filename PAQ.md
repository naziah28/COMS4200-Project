### Guide to the abyss called oflops
### Potentially asked question log

#### Other references?

Openflow v1.0 specification document, Appendix A section for openflow protocol used by the test module.

#### How does this test work?

This will call `start` right after initializing the test module, then `handle_packet_generation`.

When we want the test to end we need to call `oflops_end_test`, usually scheduled by a timer event to avoid(?) crashing functions.

The test primarily uses a timer to schedule events. We can schedule timer event, but it can handle pcap, snmp event and "other" events.

It's our responsibility to schedule events. IMO easiest way to do it is schedule only timer event, use pcap event 
to measure data and display it before ending the test, which is the way it's done in `port_stat_request`.

#### How to generate packets?

Use `add_traffic_generator(struct oflops_context, int, struct traf_gen_det*)` declared in `oflops/traffic_generator.h`.

This implementation sends packet at regular interval specified in `traf_gen_det.delay` and set all data bits to zero.

For specific implementation, can use example in `simple_forwarding` folder or `oflops/example_modules/openflow_flow_dump_test`

We can set `traf_gen_det.pkt_size` to do one of our test. 

#### How to measure data?

Either manually (as "demonstrated" in `port_stat_request`) or use Simple Network Management Protocol (SNMP) ("demonstrated" in `simple_forwarding`).

SNMP should work better, look at `handle_snmp_event` in `simple_forwarding` for details.

#### How to set flow rules?

We need to setup flow rules at the start of the test module (a.k.a in `start`). `struct flow` is declared in `oflops/msg.h`

Then we have to add the flow to the switch by `make_ofp_flow_add`. We will have to specify the output port of the flow and possibly 
make a pcap filter for the packets that come through.

For specific implementation, can use example in `simple_forwarding` folder or `oflops/example_modules/openflow_flow_dump_test`

#### How to send control channel message?

Use either `oflops_send_of_msg` defined in `oflops/test_modules.h` or `make_ofp_*` defined in `msg.h`.

`make_ofp_*` requires a `void*` to store the data, needs to manually send using `write` to control channel file descriptor, 
and also need to free it right after.

For messages requiring a body, **I think** it's better using `oflops_send_of_msg`. Consult openflow v1.0 spec for specific details 
(which struct should be in body etc.).

#### How to dissect packets?

For data channel packets, use `extract_pktgen_pkt` declared in `traffic_generator.h`.

For control channel packets, cast `pe->data` to `struct ofp_header *` and go from there. Use `type` to know which type of message it is.
I'll try to change the one in `port_stat_request`.

#### Debug?

As this in a module, injecting `printf` statements inside the code should work. 

Running wireshark on the specified interface when running `oflops` also works as well.

You might need to get root permission for wireshark (either by `sudo wireshark` or get root by `sudo -s` and run `/usr/bin/wireshark`).

#### Can I get a new folder for my test?

Copy the template folder, rename parts that contains `template` to whatever you need.

Then change `scripts/configure.ac` to include the path to your folder's Makefile.

Then run `scripts/setup.sh`, which will hopefully do the trick (sorry Naziah + Jake) or put out the instructions.

