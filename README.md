# COMS4200 Project

  A project about testing OpenFlow compatible switches.

## OvS(-DPDK) switch build

This build instruction is on with OvS 2.6.0 and DPDK 16.11.1 LTS on Ubuntu 14.04 with kernel 4.4.0-31-generic.

Most of these commands require root privilege, i.e. `sudo`.

1. Prepare necessary files

```
mkdir COMS4200 & cd COMS4200
sudo apt-get install build-essential automake docker.io vim kmod python python-pip libtool
sudo pip install -U pip six
wget http://dpdk.org/browse/apps/pktgen-dpdk/snapshot/pktgen-dpdk-pktgen-3.1.1.tar.gz
wget http://fast.dpdk.org/rel/dpdk-16.11.1.tar.xz
wget http://openvswitch.org/releases/openvswitch-2.6.1.tar.gz
tar -xf dpdk-16.11.1.tar.xz
tar -xzf openvswitch-2.6.1.tar.gz
tar -xzf pktgen-dpdk-pktgen-3.1.1.tar.gz
```

2. Export environment variables for installation
Note that these variables will be different depending on the machine.

```
export RTE_TARGET="x86_64-native-linuxapp-gcc"
export DPDK_DIR="/home/sdn/COMS4200/dpdk-stable-16.11.1"
export DPDK_BUILD=$RTE_TARGET
export OVS_DIR="/home/sdn/COMS4200/openvswitch-2.6.1"
```

3. Build DPDK

```
cd $DPDK_DIR
make config O="$DPDK_BUILD" T="$DPDK_BUILD"
cd $DPDK_BUILD
make -j8
```

4. Build OvS

```
cd $OVS_DIR
./boot.sh
CFLAGS='-march=native' ./configure --with-dpdk="$DPDK_DIR"/"$DPDK_BUILD"
make -j8
```

## Testing
  
### Prerequisites

The test module requires an installation of [OpenFlow](https://github.com/mininet/openflow) and [oflops](https://github.com/mininet/oflops). Both of them can be obtained through a standard installation of Mininet.

#### Writing tests

`oflops` is the API used for testing. All files in the folder are relevant to the test. A good starting file is `test_module.h`

#### Building tests

This project uses the configuration of `oflops`. Clone this directory under `oflops`, then just run `setup.sh`. This will change the build of `oflops` to contain the tests included here. A single build of oflops will generate the Makefiles necessary to recompile the test.

#### Running tests

We will need to run this in two separate terminal. One will run the test and one will run the switch under test.

- Run switch: Either OvS or OvS-DPDK will be ran in the background to wait for controller connection. Those commands will setup a datapath between two interfaces: `veth0` and `veth1`, controller interface to a localhost TCP port 42069, and runs OpenFlow protocol v1.0 on localhost TCP port 6653.

OvS:
```
sudo -s
source path/to/ovs-launch.sh
```

OvS-DPDK:
```
sudo -s
source path/to/ovs-dpdk-launch.sh

```

- Run `oflops` test: Running `oflops` requires the datapath and controller port specified above, along with a built test module. Modules files are `.so` files in their respective test folders' `.libs` folder.

Using OvS switch:
```
sudo oflops -c lo -d veth0 -d veth1 -p 6653 -o output.txt path/to/module/file
```

Using OvS-DPDK switch:
```
sudo oflops -c lo -d dpdk0 -d dpdk1 -p 6653 -o output.txt path/to/module/file
```

For example, to run `simple_forwarding` from the repo directory (with either OvS or OvS-DPDK switch running in the background): 
```
sudo oflops -c lo -d veth0 -d veth1 -p 6653 simple_forwarding/.libs/lib_simple_forwarding.so

```