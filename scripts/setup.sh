#!/bin/sh

usage="Usage: setup.sh [oflops_directory]\n Requires the repository to be located under oflops"

if [ $# -ne 0 ]; then
    if [ $# -eq 1 ]; then
	repoDir=$1"/COMS4200-Project"
	echo "Expected repo directory" $repoDir
	if [ -d $repoDir ]; then
	    # Copy of the working configure/automake file
	    cp Makefile.am $1
	    cp configure.ac $1

	    # Just to be sure that we have the up-to-date Makefile
	    cd $1
	    ./boot.sh
	    ./configure
	    exit 1
	else
	    echo $usage
	fi
    else
	echo $usage
	exit 1
    fi
fi

# Some of our team's directory to not bother with the path every time we need to setup new tests
if [ -d "/home/sdn/oflops/COMS4200-Project" ] ;
then
    # Copy of the working configure/automake file
    cp Makefile.am /home/sdn/oflops
    cp configure.ac /home/sdn/oflops

    # Just to be sure that we have the up-to-date Makefile
    cd /home/sdn/oflops
    ./boot.sh
    ./configure
    exit 1
fi

# Naziah review pls
if [ -d "/home/parallels/sdn/oflops/COMS4200-Project" ] ;
then
    # Copy of the working configure/automake file
    cp Makefile.am /home/parallels/sdn/oflops
    cp configure.ac /home/parallels/sdn/oflops

    # Just to be sure that we have the up-to-date Makefile
    cd /home/parallels/sdn/oflops
    ./boot.sh
    ./configure
    exit 1
fi
