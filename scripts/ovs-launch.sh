#!/bin/bash

echo "Launching OVS..."
usage="Usage: ovs-launch.sh [OPENFLOW_DIR]\nRuns as root only"

if [ "$(id -u)" != "0" ]; then
	echo "Sorry, this script requires root permission"
        exit 1
fi

echo "This somehow only works when run as root, not sudo"
echo "Use 'sudo -s' to become root at current directory"

# Environment variables
echo "Setting up OpenFlow directory"
OPENFLOW_DIR=""

if [ -d "/home/sdn/openflow/secchan" ]; then
    OPENFLOW_DIR="/home/sdn/openflow"
elif [ -d "/home/parallels/sdn/openflow/secchan" ];
then
    OPENFLOW_DIR="/home/parallels/sdn/openflow"
fi

if [ $# -eq 1 ]; 
then
    if [ -d $1 ]; then
	OPENFLOW_DIR=$1
    fi
    if [ $OPENFLOW_DIR == "" ]; then
	echo $usage
	exit 1
    fi
elif [ $# -ne 0 ];
then
    echo $usage
    exit 1
fi

echo "Terminating Open vSwitch processes"
pkill -9 ovs
pkill -9 ofdatapathi
pkill -9 pmd

echo "Removing desired virtual interfaces"
ip link delete veth0
ip link delete veth2

echo "Removing directories used by Open vSwitch"
rm -rf /usr/local/var/run/openvswitch
rm -rf /usr/local/etc/openvswitch/
rm -f /tmp/conf.db
rm -f /tmp/ofd

echo "Unmounting hugepages"
umount nodev /mnt/huge

echo "Deleting huge page memory files..."
rm -rf /dev/hugepages/*

echo "Show the hugepage mounts"
mount | grep -i huge	

echo "De-allocating hugepages"
echo 0 > /sys/kernel/mm/hugepages/hugepages-2048kB/nr_hugepages

echo "Show free/used hugepage info"
cat /proc/meminfo | grep -i huge

echo "Removing from the kernel DPDK drivers"
echo "Please work. This one I literally don't know why"
rmmod igb_uio
rmmod cuse
rmmod fuse
rmmod openvswitch
rmmod uio
rmmod eventfd_link
rmmod ioeventfd
	
echo "Finish clean up"
echo "Setting up OvS"

echo "Disabling IPv6 for the test"
sysctl -w net.ipv6.conf.all.disable_ipv6=1

echo "Configuring IP address for virtual interfaces"
ip link add name veth0 type veth peer name veth1
ip link add name veth2 type veth peer name veth3
# Or just Bash loop
ifconfig veth0 up
ifconfig veth1 up
ifconfig veth2 up
ifconfig veth3 up
ifconfig veth0 192.128.10.1 netmask 255.255.255.0
ifconfig veth1 192.168.11.1 netmask 255.255.255.0
ifconfig veth2 192.168.12.1 netmask 255.255.255.0
ifconfig veth3 192.168.13.1 netmask 255.255.255.0

DATAPATH=$OPENFLOW_DIR"/udatapath/ofdatapath"
OFPROTOCOL=$OPENFLOW_DIR"/secchan/ofprotocol"
if [ -f $DATAPATH ] && [ -f $OFPROTOCOL ]; 
then
    echo "Creating datapath between veth0 and veth1"
    $DATAPATH -i veth0 punix:/tmp/ofd --detach
    echo "Starting ofprotocol on port 6653"
    $OFPROTOCOL unix:/tmp/ofd tcp:127.0.0.1:6653 --fail=closed --max-backoff=8
else
    echo "Could not find either datapath or ofprotocol in OpenFlow folder"
    exit 1
fi
