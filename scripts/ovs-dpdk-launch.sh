#!/bin/bash

echo "Launching OVS-DPDK..."
usage="Usage: ./ovs-dpdk-launch.sh [OVS_DIR OPENFLOW_DIR DPDK_DIR]\nRuns as root only"

if [ "$(id -u)" != "0" ]; then
	echo "Sorry, this script requires root permission"
        exit 1
fi

echo "This somehow only works when run as root, not sudo"
echo "Use 'sudo -s' to become root at current directory"

# Environment variables
DPDK_BUILD="x86_64-native-linuxapp-gcc"

echo "Setting up OpenFlow, OvS and DPDK directory"
OPENFLOW_DIR=""
OVS_DIR=""
DPDK_DIR=""
if [ -d "/home/sdn/COMS4200/openvswitch-2.6.1" ]; then
    OVS_DIR="/home/sdn/COMS4200/openvswitch-2.6.1"
elif [ -d "/home/parallels/sdn/COMS4200/openvswitch-2.6.1" ];
then
    OVS_DIR="/home/parallels/sdn/COMS4200/openvswitch-2.6.1"
fi

if [ -d "/home/sdn/openflow/secchan" ]; then
    OPENFLOW_DIR="/home/sdn/openflow"
elif [ -d "/home/parallels/sdn/openflow/secchan" ];
then
    OPENFLOW_DIR="/home/parallels/sdn/openflow"
fi

if [ -d "/home/sdn/COMS4200/dpdk-stable-16.11.1" ]; then
    DPDK_DIR="/home/sdn/COMS4200/dpdk-stable-16.11.1"
elif [ -d "/home/parallels/sdn/COMS4200/dpdk-stable-16.11.1" ];
then
    DPDK_DIR="/home/parallels/sdn/COMS4200/dpdk-stable-16.11.1"
fi

if [ $# -eq 3 ]; 
then
    if [ -d $1 ]; then
	OVS_DIR=$1
    fi
    if [ -d $2"/secchan" ]; then
        OPENFLOW_DIR=$2
    fi
    if [ -d $3 ]; then
	DPDK_DIR=$3
    fi
    if [ $OVS_DIR == "" ] || [ $OPENFLOW_DIR == "" ] || [ $DPDK_DIR == "" ]; then
	echo $usage
	exit 1
    fi
elif [ $# -ne 0 ];
then
    echo $usage
    exit 1
fi

echo "Terminating Open vSwitch processes"
pkill -9 ovs
# Just being paranoid
pkill -9 pmd
pkill -9 ofdatapath

echo "Removing desired virtual interfaces"
ip link delete veth0
ip link delete veth2

echo "Removing directories used by Open vSwitch"
rm -rf /usr/local/var/run/openvswitch
rm -rf /usr/local/etc/openvswitch/
rm -f /tmp/conf.db
rm -f /tmp/ofd

echo "Unmounting hugepages"
umount nodev /mnt/huge

echo "Deleting huge page memory files..."
rm -rf /dev/hugepages/*

echo "Show the hugepage mounts"
mount | grep -i huge	

echo "De-allocating hugepages"
echo 0 > /sys/kernel/mm/hugepages/hugepages-2048kB/nr_hugepages

echo "Show free/used hugepage info"
cat /proc/meminfo | grep -i huge

echo "Removing from the kernel DPDK drivers"
echo "Please work. This one I literally don't know why"
rmmod igb_uio
rmmod cuse
rmmod fuse
rmmod openvswitch
rmmod uio
rmmod eventfd_link
rmmod ioeventfd
	
echo "Finish clean up"
echo "Setting up OVS-DPDK"

mkdir -p /mnt/huge

echo "Disabling IPv6 for the test"
sysctl -w net.ipv6.conf.all.disable_ipv6=1

echo "Allocating huge pages"
echo 2048  > /sys/kernel/mm/hugepages/hugepages-2048kB/nr_hugepages

mkdir -p /usr/local/etc/openvswitch
mkdir -p /usr/local/var/run/openvswitch
mount -t hugetlbfs none /mnt/huge
modprobe uio
insmod $DPDK_DIR/$DPDK_BUILD/kmod/igb_uio.ko

echo "Showing hugepages info"
mount | grep -i huge

echo "Showing free/used hugepages info"
cat /proc/meminfo | grep -i huge

echo "Configuring OVS database"
cd $OVS_DIR
./ovsdb/ovsdb-tool create /usr/local/etc/openvswitch/conf.db ./vswitchd/vswitch.ovsschema
./ovsdb/ovsdb-server --remote=punix:/usr/local/var/run/openvswitch/db.sock --remote=db:Open_vSwitch,Open_vSwitch,manager_options --pidfile --detach

echo "Starting Open vSwitch daemon with DPDK support"
./utilities/ovs-vsctl --no-wait set Open_vSwitch . other_config:dpdk-init=true other_config:dpdk-lcore-mask=0x2 other_config:dpdk-socket-mem="512"
./utilities/ovs-vsctl --no-wait init

./vswitchd/ovs-vswitchd unix:/usr/local/var/run/openvswitch/db.sock --pidfile --detach

echo "Adding bridge/datapath br0 with interfaces veth0 and veth1"
./utilities/ovs-vsctl --no-wait add-br br0
# DPDK datapath
./utilities/ovs-vsctl --no-wait set bridge br0 datapath_type=netdev
ip link add name veth0 type veth peer name veth1
ip link add name veth2 type veth peer name veth3
# Or just Bash loop
ifconfig veth0 up
ifconfig veth1 up
ifconfig veth2 up
ifconfig veth3 up
ifconfig veth0 192.128.10.1 netmask 255.255.255.0
ifconfig veth1 192.168.11.1 netmask 255.255.255.0
ifconfig veth2 192.168.12.1 netmask 255.255.255.0
ifconfig veth3 192.168.13.1 netmask 255.255.255.0

./utilities/ovs-vsctl --no-wait add-port br0 veth0
# DPDK interfaces will not responding to PMD.
./utilities/ovs-vsctl --no-wait set Interface dpdk0 type=dpdk options:dpdk-devargs=eth_null0 
./utilities/ovs-vsctl --no-wait add-port br0 veth1 
./utilities/ovs-vsctl --no-wait set Interface dpdk1 type=dpdk options:dpdk-devargs=eth_null1

echo "Setting control channel to local tcp port 42069"
echo "Haha funny number"
./utilities/ovs-vsctl --no-wait set-controller br0 tcp:127.0.0.1:42069

cd $OPENFLOW_DIR"/secchan"
echo "Starting ofprotocol between tcp port 42069 and tcp port 6653"
./ofprotocol tcp:127.0.0.1:42069 tcp:127.0.0.1:6653 --fail=closed --max-backoff=8
