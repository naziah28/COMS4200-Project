#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <sys/socket.h>
#include <sys/types.h>
#include <sys/time.h>

#include <arpa/inet.h>

#include <test_module.h>

#define PACKET_IN_DEBUG 0

#define WRITEPACKET "write packet"
#define ENDTEST "end test"

#ifndef BUFLEN
#define BUFLEN 4096
#endif

int rate = 512;
int max_num_packet;
struct timeval starttime, endtime;

// OpenFlow packet buffer
struct ofp_stats_request* buf = NULL;
struct ofp_port_stats_request* msgbody = NULL;

// Index of next control channel message
int next_index = 0;
// Send counter
uint64_t sentcounter = 0;
// Captured packets through pcap
uint64_t pcap_sentcounter = 0;

// Receive counter
uint64_t receivecounter = 0;

// Module name
char* name()
{
	return "port_status_spam";
}

// Just comparing timestamps
double diff_ms(struct timeval t1, struct timeval t2)
{
    return ((double) ((t1.tv_sec - t2.tv_sec) * 1000000) + 
            (t1.tv_usec - t2.tv_usec))/1000;
}

/** Initialization
 * @param ctx pointer to opaque context
 */
int start(struct oflops_context * ctx)
{
	struct timeval now;
	struct ofp_header ofph;
	gettimeofday(&now,NULL);
	now.tv_sec += 5;
	
	// Starting test in 5 seconds
	oflops_schedule_timer_event(ctx,&now, WRITEPACKET);

	// send a friendly hello
	ofph.length = htons(sizeof(struct ofp_header));
	ofph.xid = 0;
	ofph.type = OFPT_HELLO;
	ofph.version = OFP_VERSION;
	oflops_send_of_mesg(ctx,&ofph);

	// Prepare message body
	msgbody = malloc(sizeof(struct ofp_port_stats_request));
	// Stats for all ports
	msgbody->port_no = OFPP_NONE;

	// build port stat request
	// Packet type is 4: Openflow Port Status
	buf = malloc(sizeof(struct ofp_stats_request) + sizeof(struct ofp_port_stats_request));
	
	buf->header.length = htons(sizeof(struct ofp_stats_request) + sizeof(struct ofp_port_stats_request));
	buf->header.xid = 0;
	buf->header.type = OFPT_STATS_REQUEST;
	buf->header.version = OFP_VERSION;
	buf->type = htons(OFPST_PORT); 
	buf->flags = 0;
	memcpy(buf->body, msgbody, sizeof(struct ofp_port_stats_request));

	max_num_packet = rate * 10;
	// Test begins on first packet sent
	return 0;
}

/** Handle timer event
 * @param ctx pointer to opaque context
 * @param te pointer to timer event
 */
int handle_timer_event(struct oflops_context * ctx, struct timer_event *te)
{
	struct timeval now;
	char* str;

	gettimeofday(&now,NULL);
	str = (char *) te->arg;
	// if (PACKET_IN_DEBUG)
	// 	fprintf(stderr, "At %ld.%.6ld (sched for %ld.%.6ld) : Got timer_event %s \n",
	// 			now.tv_sec, now.tv_usec, te->sched_time.tv_sec, te->sched_time.tv_usec, str);

	if(!strcmp(str,WRITEPACKET))
	{
		//Send message
		if (sentcounter == 0)
		{
			gettimeofday(&starttime, NULL);
			fprintf(stderr, "Start...\n");
		}
		buf->header.xid = next_index;
		if (PACKET_IN_DEBUG)
			fprintf(stderr, "Sending message %lld\n", (long long int)sentcounter);
		next_index++;
		oflops_send_of_mesg(ctx,(struct ofp_header *) buf);
		sentcounter++;
		//Schedule next one
		te->sched_time.tv_usec += 1000000/rate;	
		if(te->sched_time.tv_usec >= 1000000)
		{
			te->sched_time.tv_sec++;
			te->sched_time.tv_usec-=1000000;
		}
		// timercmp(&endtime, &te->sched_time, >)
		if(sentcounter < max_num_packet)
			// have time to fire off another probe
			oflops_schedule_timer_event(ctx,&te->sched_time, WRITEPACKET);
		else
		{
			te->sched_time.tv_sec += 5;	// give some time just to make sure to receive everything
			// End time does not contain the waiting time for packets
			gettimeofday(&endtime, NULL);
			oflops_schedule_timer_event(ctx,&te->sched_time, ENDTEST);
		}
	}
	else if (!strcmp(str,ENDTEST)) {
		gettimeofday(&now,NULL);
		//End experiment
		fprintf(stderr, "Experiment has %lld packets sent and %lld received -- %f dropped\n",
			(long long int)sentcounter, (long long int)receivecounter , 
			(long long int)(sentcounter-receivecounter)/(float)sentcounter);
		fprintf(stderr, "Elapsed: %3f\n", diff_ms(endtime, starttime));
		free(msgbody);
		free(buf);
		sleep(2);
		oflops_end_test(ctx,1);
		return 0;
	} else {
		fprintf(stderr, "Unknown timer event with arg: %s", str);
	}
		
	return 0;
}


/** Handle timer event
 * @param ctx pointer to opaque context
 * @param pkt_in pointer to packet in event
 */
int of_event_packet_in(struct oflops_context *ctx, struct ofp_packet_in * pkt_in)
{
  return 0;
}

/** Register pcap filter.
 * @param ctx pointer to opaque context
 * @param ofc enumeration of channel that filter is being asked for
 * @param filter filter string for pcap
 * @param buflen length of buffer
 */
int get_pcap_filter(struct oflops_context *ctx, oflops_channel_name ofc, char * filter, int buflen)
{
  if(ofc == OFLOPS_CONTROL)	
    // pcap on the control channel only
    return snprintf(filter,buflen,"tcp port 6653");
  else 
    // no pktgen module here
    return 0;
}

/** Handle pcap event.
 * @param ctx pointer to opaque context
 * @param pe pcap event
 * @param ch enumeration of channel that pcap event is triggered
 */
int handle_pcap_event(struct oflops_context *ctx, struct pcap_event * pe, oflops_channel_name ch)
{
	// Literally packet dissection
	uint8_t type;
	struct timeval ptime;
	int probe_index;
	if (ch != OFLOPS_CONTROL)
	{
		fprintf(stderr, "wtf! why channel %u?", ch);
		return 0;
	}
	//See packet received
	// Looks through packet dissection in the meantime
	// why 70
	if(pe->pcaphdr.caplen <70) 
		return 0;	// not for us
	type = pe->data[67];
	if(type != OFPT_STATS_REQUEST && type != OFPT_STATS_REPLY)
		return 0;
	ptime = pe->pcaphdr.ts;
	probe_index  = *((uint32_t*) &(pe->data)[70]);
	// fprintf(stderr, "Found unmarked probe.");
	if (PACKET_IN_DEBUG)
		fprintf(stderr, "Got OpenFlow packet of length %u type %u at %ld.%.6ld of probe_index %d\n", 
				pe->pcaphdr.len, type,
				ptime.tv_sec, ptime.tv_usec, 
				probe_index);

	//Handle stat request and stat reply
	if (type == OFPT_STATS_REQUEST)
	{
		//Record sending
		pcap_sentcounter++;
		if (PACKET_IN_DEBUG)
			fprintf(stderr, "Send stat request at %ld.%.6ld of probe_index %d\n", 
					ptime.tv_sec, ptime.tv_usec, 
					probe_index);
	}
	else if (type == OFPT_STATS_REPLY)
	{
		receivecounter++;
		if (PACKET_IN_DEBUG)
			fprintf(stderr, "Receive stat reply at %ld.%.6ld of probe_index %d\n", 
					ptime.tv_sec, ptime.tv_usec, 
					probe_index);

		// Did I receive everything?

		// if (timediff.tv_sec != 0)
		// {
		// 	fprintf(stderr, "Delay of > 1 sec!\n");
		// 	return 0;
		// }
	

		if (PACKET_IN_DEBUG)
		{
			fprintf(stderr, "Got stat of probe_index %d\n", 
					probe_index);
			fprintf(stderr, "\twith %lld packets sent and %lld received.\n", 
					(long long int)sentcounter, (long long int)receivecounter);
		}
	} else {
		if (PACKET_IN_DEBUG) {
			fprintf(stderr, "Got message with type %d\n", type);
		}
	}
	return 0;
}



