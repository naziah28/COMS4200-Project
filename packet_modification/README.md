**PACKET MODIFICATION TEST**

The default duration of the test is 20. We can set out own duration with the args "duration=\<numberofseconds>". However, if the test runs for more than 27-30 seconds, it might get switch connection reset error.

Example:

```
sudo oflops -c lo -o oflops.log -d veth1 -d veth3 -p 6653 ./.libs/libopenflow_packet_modification.so duration=20
```

