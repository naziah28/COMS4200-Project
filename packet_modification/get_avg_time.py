import pandas as pd 

# get average message send times for each trial 
for t in range(9):
	try: 
		df = pd.read_csv(f"trial-{t}/action_aggregate.log", sep=';', names=['message_no', 'send_time','receive_time','channel'])
		df['send_duration'] = (df['receive_time']-df['send_time'])
		print(df.shape)
		print(f"trial-{t}", df['send_duration'].mean())
	except: 
		pass
